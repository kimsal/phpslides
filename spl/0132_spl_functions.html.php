<h2 class="title">
SPL - Functions
</h2>
<p>
We can inspect an class/object and tell what it's based on with class_implements.
</p>
<pre class="code php parse">
$x = new SimpleXMLIterator("&lt;root>&lt;data>fff&lt;/data>&lt;/root>");
print_r(class_implements($x));
echo "Number of elements in x = ".count($x);
</pre>
