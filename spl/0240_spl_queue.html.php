<h2 class="title">
SPL - Queues
</h2>
<p>
Queues and Stacks are provides as ways of standardizing FIFO/LIFO 
behaviour in an OO manner.
</p>
<pre class="code php parse">
/**
 * SPL Queue and Stack are subclasses of DoublyLinkedList
 */
echo "Create Object of Spl. Queue:";
$obj = new SplQueue();

echo "\nCheck for Queue is Empty:";
if($obj->isEmpty())
{
    $obj->enqueue("Simple");
    $obj->enqueue("Example");
    $obj->enqueue("Of");
    $obj->enqueue("PHP");
}

echo "\nView queue:";
print_r($obj);

if(! $obj->offsetExists(4))
{
    $obj->enqueue(10);
}

print_r($obj);


</pre>
