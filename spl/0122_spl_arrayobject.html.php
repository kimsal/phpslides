<h2 class="title">
SPL - ArrayObject
</h2>
<p>
The built-in SimpleXML object is based on an ArrayObject. You've 
probably used this already without even realizing it!
</p>

<pre class="code php parse">
<?php 
$x = simplexml_load_string('
<root>
<row id="4">foo</row>
<row id="2">bar</row>
</root>
');
echo $x->row[1]['id']; // we're accessing a simplexml *object* with array syntax
?>
</pre>
