<h2 class="title">
SPL - Recursive Iterator
</h2>
<p>
Implement a recursive iterator
</p>

<pre class="code php">
<?php
class MyRecursiveIterator implements RecursiveIterator
{
    private $_data;
    private $_position = 0;
    
    public function __construct(array $data) {
        $this->_data = $data;
    }
    
    public function valid() {
        return isset($this->_data[$this->_position]);
    }
    
    public function hasChildren() {
        return is_array($this->_data[$this->_position]);
    }
    
    public function next() {
        $this->_position++;
    }
    
    public function current() {
        return $this->_data[$this->_position];
    }
    
    public function getChildren() {
        echo "<pre>";
        print_r($this->_data[$this->_position]);
        echo "</pre>";
    }
    
    public function rewind() {
        $this->_position = 0;
    }
    
    public function key() {
        return $this->_position;
    }
}

$arr = array(0, 1, 2, 3, 4, 5 => array(10, 20, 30), 6, 7, 8, 9 => array(1, 2, 3));
$mri = new MyRecursiveIterator($arr);

foreach ($mri as $c => $v) {
    if ($mri->hasChildren()) {
        echo "$c has children: <br />";
        $mri->getChildren();
    } else {
        echo "$v <br />";
    }

}
?>
</pre>
<h2>Output</h2>
<pre class="output">
0 <br />1 <br />2 <br />3 <br />4 <br />5 has children: <br /><pre>Array
(
    [0] => 10
    [1] => 20
    [2] => 30
)
</pre>6 <br />7 <br />8 <br />9 has children: <br /><pre>Array
(
    [0] => 1
    [1] => 2
    [2] => 3
)
</pre>
</pre>