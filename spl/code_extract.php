<?php
@mkdir("./code/");
$f = glob("./code/*");
foreach($f as $file) {
	unlink($file);
}


$f = glob("*html");
foreach($f as $file)
{
	list($name, $junk) = explode("_",$file);

	$temp= file_get_contents($file);
	$temp = str_replace("<?","__open__",$temp);
	$temp = str_replace("?>","__close__",$temp);

	$xml = simplexml_load_string("<?xml version='1.0'?><html>$temp</html>");
	$codes = $xml->xpath("//pre[@class='code php']");
	foreach($codes as $num=>$code) {
		$text = str_replace("__open__","<?", $code);
		$text = str_replace("__close__","?>", $text);
		file_put_contents("./code/$name"."_code_".($num+1).".php", trim($text) );
	}
}
?>
