<h2 class="title">
SPL - Functions
</h2>
<p>
Iterators are a way of standardizing your iteration through a list.  
However, sometimes you still need a generic array.  <b>iterator_to_array()</b> to the rescue!
</p>
<p>They don't always work exactly as you'd think though!</p>

<pre class="code php parse">
$x = new SimpleXMLIterator("&lt;root>&lt;data>fff&lt;/data>&lt;/root>");
print_r($x);
$a = iterator_to_array($x);
print_r($a);
</pre>
