<h2 class="title">
SPL - DoublyLinkedList
</h2>
<p>
SPL provides an implementation of a doubly-linked-list, a way of storing data 
in a tree format.  Each node has an internal link to the previous and next node.  Finding 
values in a DLL can be faster than regular array access.
</p>
<pre class="code php parse">
$dll = new SplDoublyLinkedList();
$a = array();
$m1 = memory_get_usage();
for($x=0; $x&lt;1000; $x++) {
array_push($a, $x);
}

$m2 = memory_get_usage();
for($x=0; $x&lt;1000; $x++) {
$dll->push($x);
}
$m3 = memory_get_usage();
echo "Memory for array version:\nend=$m2\nstart=$m1\nused=".($m2-$m1)."\n";
echo "Memory for DLL version:\nend=$m3\nstart=$m2\nused=".($m3-$m2)."\n";
</pre>
