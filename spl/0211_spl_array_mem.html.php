<h2 class="title">
SPL - FixedArray Memory
</h2>
<p>
When the number of elements is moderate (>100?), an SplFixedArray 
will likely offer at least small performance and/or memory benefits vs regular arrays.
</p>

<h2>Memory example</h2>
<pre class="code php">
<?php 
$number = 200;
$string = "this is a short string";
$mem1 =  memory_get_usage(); 
for($x=0; $x<$number; $x++) { 
	$array1[$x] = microtime()." and other random text ".rand(5,20202020);
}
echo "standard array used ".(memory_get_usage()-$mem1)."\n";

$mem2 =  memory_get_usage(); 
$spl = new SplFixedArray($number);
for($x=0; $x<$number; $x++) { 
	$spl[$x] = microtime()." and other random text ".rand(5,20202020);
}
echo "SPL fixed array used ".(memory_get_usage()-$mem2)."\n";
?>
</pre>
<h2>Output is</h2>
<pre>
standard array used 28116
SPL fixed array used 19216 (roughly 30% savings)
</pre>