<h2 class="title">
SPL - DoublyLinkedList
</h2>
<p>
The DLL has a number of options - for example, we can very conveniently 
delete the items as we iterate over them, freeing memory as we go.  
</p>
<pre class="code php parse">
/**
 * NOTE _ DLL is not circular list
 */

$dll = new SplDoublyLinkedList();
for($x=0; $x&lt;1000; $x++) {
	$dll->push($x);
}
$m1 = memory_get_usage(0);

/**
 * iterate over - don't do anything
 */
foreach($dll as $val) { }
$m2 = memory_get_usage(0);
echo "Memory usage: ".($m2-$m1)."\n";
echo "DLL has entry count: ".count($dll)."\n";

/**
 * set iterator mode to delete as we iterate
 */
$dll->setIteratorMode(SplDoublyLinkedList::IT_MODE_DELETE);

/**
 * iterate over - don't do anything
 */
foreach($dll as $val) { }
$m3 = memory_get_usage(0);
echo "DLL has entry count: ".count($dll)."\n";
echo "Memory usage: ".($m3-$m2)."\n";



</pre>
