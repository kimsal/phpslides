<h2 class="title">
SPL - Heap
</h2>
<p>
We can create our own heap and use a custom comparator.  This isn't really 
all that different from the maxheap.  :)
</p>
<pre class="code php parse">
class myheap2 extends SplHeap {
    public function  compare( $v1, $v2 ) {
        return ($v1 - $v2); // positive 0 or negative determines sortin
    }
}
$h = new myheap2();
for($x=0; $x&lt;5; $x++) { 
$h->insert((int)rand(5,5000));
}
print_r($h);
while($h->valid())
{
	print_r($h->current()); echo "\n";
	$h->next();
}
</pre>
