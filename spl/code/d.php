<?php
interface phone {
	public function makeCall($number);
	public function answerCall();
}
interface camera {
	public function takePicture();
}
interface browser {
	public function gotoURL($url);
}

class basePhone implements phone {
	public function makeCall($number) { echo "calling $number"; }
	public function answerCall() { echo "Hello - leave a message!"; }
}

class cameraPhone extends basePhone implements camera {
	public function takePicture() { echo "taking picture!"; }
}

class smartPhone extends cameraPhone implements browser {
	public function gotoURL($url) { 
		return file_get_contents($url);
	}
}

// we can type hint on the *interface type* as well
// as with a regular class type

function getURLContents(browser $browser, $url) {
	echo $browser->gotoURL($url);
}

$p = new smartPhone();
getURLContents($p, "http://yahoo.com");

$b = new basePhone();
getURLContents($b, "http://yahoo.com"); // will error out

