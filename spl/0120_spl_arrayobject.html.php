<h2 class="title">
SPL - ArrayObject
</h2>
<p>
PHP provides a built-in object which can hold an array of information, but allow the data 
to be accessed as an object.
</p>
<pre class="code php parse">
<?php
$array = new ArrayObject(array("a","b","c"));
echo $array[0]."\n";
$array[55] = "ggg";
print_r($array);
?>
</pre>

