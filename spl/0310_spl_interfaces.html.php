<h2 class="title">
SPL - Countable
</h2>
<p>
Implement the countable interface to allow your objects to respond 
to the count() function.
</p>

<pre class="code php parse">
<?php
class people implements countable {
	
	protected $items = array();
	
	public function addItem($i) {
		$this->items[] = $i;
	}
	
	public function count()
	{
		return count($this->items);
	}
}
$x = new people();
$x->addItem(256);
$x->addItem(512);
echo count($x); // 2
?>


</pre>