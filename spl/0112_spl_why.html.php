<h2 class="title">
SPL - Standard PHP Library 
</h2>
<h2>Why should I care?</h2>
<p>
In some cases, using SPL-specific data structures can save you a
significant amount of memory compared to the tried-and-true 
PHP default structures.
</p>
<p>
Does a 30% reduction in memory usage 
for data structures sound good to you?
</p>
