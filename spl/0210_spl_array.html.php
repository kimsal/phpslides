<h2 class="title">
SPL - FixedArray
</h2>
<p>
PHP's SplFixedArray offers a way to create a fixed size array which takes 
only integers as keys.  Creation time of the array is reduced vs continually adding on 
elements to a generic PHP array.
</p>
<pre class="php">
<?php
$array = new SplFixedArray(20); // creates 0-based index from 0-19
$array[4] = "Hello!";
echo $array->getSize(); // 20
$array[20] = "Foo"; // runtime exception - index out of bounds!
?>
</pre>
