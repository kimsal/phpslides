<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  
  <!--[if lt IE 9]>
  <script src="../lib/html5shim.js"></script>
  <![endif]-->  
  
  <!-- These are some core styles the slideshow app requires -->
  <link rel="stylesheet" href="../lib/styles.css" />
  
  <!-- These are the styles you'll add to make the slides look great -->
  <link rel="stylesheet" href="../css/jquery.snippet.css" />
  <link rel="stylesheet" href="../css/custom.css" />
  
  <title>PHP SPL</title>
</head>
<body>
  <header>
    <h1>PHP SPL</h1>
    <nav>
      <ul>
        <li><button id="prev-btn" title="Previous slide">Previous Slide</button></li>
        <li><span id="slide-number"></span>/<span id="slide-total"></span></li>
        <li><button id="next-btn" title="Next Slide">Next Slide</button></li>
      </ul>
    </nav>
  </header>
  <div id="deck">

<?php
$f = glob("*html.php");
$count = 0;
foreach($f as $file)
{
$temp= file_get_contents($file);
$temp = str_replace("<?php","#_open_php",$temp);
$temp = str_replace("?>","#_close_php",$temp);
preg_match_all("/#_open_php(.*)#_close_php/ms",$temp,$m);
$new = str_replace("<","&lt;",$m[0]);
$temp = str_replace($m[0],$new,$temp);
$out = array();
$y = simplexml_load_string("<root>$temp</root>");
$code = $y->xpath("//pre[@class='code php parse']");
foreach($code as &$c) { 
	++$count;
	$phpCode = $c[0];
	if($phpCode)
	{
		$phpCode = str_replace("#_open_php","",$phpCode);
		$phpCode = str_replace("#_close_php","",$phpCode);
		ob_start();
		eval("$phpCode");
		$out[$count] = ob_get_clean();
		$c[0] = $c[0]."\n"."__REPLACE_$count"."_";
	}
}
$txt = str_replace('<?xml version="1.0"?>', '', $y->asXML());
$data = str_replace("<?","&lt;?", $txt);
$data = str_replace("?>","?&gt;", $data);
$data = str_replace("#_open_php","&lt?php",$data);
$data = str_replace("#_close_php","?>",$data);
foreach($out as $k=>$v) { 
	$data = str_replace("__REPLACE_$k"."_",	"</pre><h2>Output</h2><pre class='output'>$v",  $data);
}
?>
<section>
<?=$data;?>
</section>
<?php
}
?>

    <!-- /End slides -->
    
  </div>
  <!-- /deck -->
  <script src="../lib/jquery-1.5.2.min.js"></script>
  <script src="../lib/jquery.jswipe-0.1.2.js"></script>  
  <script src="../lib/htmlSlides.js"></script>
<script type="text/javascript" src="../lib/jquery.snippet.js"></script>
  <script>
    //Do our business when the DOM is ready for us
    $(function() {
      
      //You can trigger Javascript based on the slide number like this:
      $('html').bind('newSlide', function(e, id) { 
        switch(id) {
          case 2:
            console.log('This is the second slide.');;
            break;
          case 3:
            console.log('Hello, third slide.');
            break;
        }
$("pre.php").snippet("php",{style:"acid"});

      });

      //One little option: hideToolbar (boolean; default = false)
      htmlSlides.init({ hideToolbar: true });
      

    });
  </script>
  <div id="phpfoot">
  Michael Kimsal - <a href="http://michaelkimsal.com">michaelkimsal.com</a>
  </div>
  </body>
</html>
