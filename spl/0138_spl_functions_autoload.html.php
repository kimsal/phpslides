<h2 class="title">
SPL - autoload
</h2>
<p>
PHP's __autoload() function is called when a class is referenced 
which has not been loaded/defined yet.  
</p>
<p>
The SPL autoload functionality allows us to chain together autoload 
behaviour, giving us the ability to define our own behaviour 
without necessarily overwriting the behaviour of other code already in the system.
</p>

<pre class="code php">
function __myAutoload1($class) { 
echo "Looking for class $class in myautoload1\n";
}
function __myAutoload2($class) { 
echo "looking for $class in myautoload2\n";
}
spl_autoload_register("__myAutoload1");
spl_autoload_register("__myAutoload2");
$t = new Teacher();

</pre>

<h2>Output</h2>
<pre>
Looking for class Teacher in myautoload1
looking for Teacher in myautoload2
</pre>