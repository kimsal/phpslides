<h2 class="title">
SPL - Standard PHP Library 
</h2>
<h2>Why should I care?</h2>
<p>
Frameworks like Zend Framework are making more use of SPL functionality, 
so understanding what's availalbe in SPL will help when dealing with 
code from others.
</p>
<p>
The "standards" provided by SPL are a mixed bag - in some cases 
there's true PHP behaviour which is implemented by SPL functions.  In other 
cases, some SPL interfaces merely serve as a guide for implementing 
popular patterns properly.
</p>
