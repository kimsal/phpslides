<h2 class="title">
SPL - Heap
</h2>
<p>
The built-in heap structures provide a way of automatically sorting 
data via a compare() method as data is added.  MinHeap and MaxHeap 
will sort in a particular direction.
</p>
<pre class="code php parse">
class myminheap extends SplMinHeap { }
class mymaxheap extends SplMaxHeap { }
$min = new myminheap();
$max = new mymaxheap();
for($x=0; $x&lt;6; $x++) { 
	$r = rand(5,5000);
	$min->insert($r);
	$max->insert($r);
}
print_r($min);
print_r($max);
while($min->valid())
{
	print_r($min->current()); echo "\n";
	$min->next();
}
</pre>
